#include<math.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>
#include<limits.h>
#include<stdbool.h>

int main() {
    bool table[10] = { true };

    table[1] = false;

    assert(table[0]);
    assert(!table[1]);
    assert(table[0]);

    return 0;
}
